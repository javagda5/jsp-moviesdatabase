package com.sda.jsp.dao;

import com.sda.jsp.model.Movie;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class MoviesDao {
    public List<Movie> getAllMovies(){
        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Criteria criteria = session.createCriteria(Movie.class);
        List<Movie> movieList = criteria.list();

        transaction.commit();

        return movieList;
    }

    public void putMovie(Movie m) {
        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(m);

        transaction.commit();
        session.close();
    }
}
