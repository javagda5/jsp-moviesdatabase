package com.sda.jsp.dao;

import com.sda.jsp.model.Person;
import com.sda.jsp.model.Profession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class PersonDao {
    public List<Person> getDirectors() {
        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        Query query = session.createQuery("FROM person WHERE profession='DIRECTOR'");
        List people = query.list();
        t.commit();

        return people;
    }

    public void addMockPeople(){
        Session session = DatabaseSession.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Person p1 = new Person();
        p1.setFirstName("Marian");
        p1.setProfession(Profession.DIRECTOR);
        Person p2 = new Person();
        p2.setFirstName("Dorian");
        p2.setProfession(Profession.ACTOR);
        Person p3 = new Person();
        p3.setFirstName("Lucjan");
        p3.setProfession(Profession.DIRECTOR);
        session.save(p1);
        session.save(p2);
        session.save(p3);

        transaction.commit();
        session.close();
    }
}
