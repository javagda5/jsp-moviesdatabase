package com.sda.jsp.model;

public enum Profession {
    DIRECTOR, ACTOR, SCENEWRITER, PRODUCER
}
