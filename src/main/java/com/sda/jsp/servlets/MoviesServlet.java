package com.sda.jsp.servlets;

import com.sda.jsp.dao.MoviesDao;
import com.sda.jsp.dao.PersonDao;
import com.sda.jsp.model.Movie;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@WebServlet(urlPatterns = "/movies")
public class MoviesServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        MoviesDao dao = new MoviesDao();
        PersonDao personDao= new PersonDao();
//        personDao.addMockPeople();
        try {
            List<Movie> movies = dao.getAllMovies();

            request.setAttribute("movies", movies);
            request.setAttribute("directors", personDao.getDirectors());

            request.getRequestDispatcher("/movies.jsp").forward(request, response);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title = req.getParameter("title");
        String dir = req.getParameter("director");
        System.out.println(req.getParameter("rate"));
        Double rate = Double.parseDouble(req.getParameter("rate"));

        String releaseDate = req.getParameter("releaseDate");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        Movie m = new Movie();
        m.setName(title);
        m.setRate(rate);
        m.setReleaseDate(LocalDate.parse(releaseDate, formatter));

        MoviesDao dao = new MoviesDao();
        dao.putMovie(m);

        resp.sendRedirect("/movies");
    }
}
