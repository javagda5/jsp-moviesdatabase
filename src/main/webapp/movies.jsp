<%@ page import="com.sda.jsp.model.Movie" %>
<%@ page import="java.util.List" %>
<%@ page import="com.sda.jsp.model.Person" %>
<%@ page import="com.sda.jsp.model.Profession" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%--<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>--%>
    <jsp:include page="views/includes.jsp"></jsp:include>
</head>
<body>
<jsp:include page="views/header.jsp"></jsp:include>
<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 bg-light">
    <div class="col-md-10 p-lg-10 mx-auto my-10">
        <h1 class="font-weight-normal">Movies</h1>

        <div class="row col-lg-12">
            <div class="col-md-1"><h4>Id</h4></div>
            <div class="col-md-3"><h4>Name</h4></div>
            <div class="col-md-1"><h4>Rate</h4></div>
            <div class="col-md-2"><h4>Director</h4></div>
            <div class="col-md-3"><h4>Release Date</h4></div>
        </div>
        <%
            List<Movie> movies = (List<Movie>) request.getAttribute("movies");
            StringBuilder builder = new StringBuilder();

            int i = 0;
            for (Movie m : movies) {
                if (i % 2 == 0) {
                    builder.append("<div class=\"row white\">");
                } else {
                    builder.append("<div class=\"row black\">");
                }
                builder.append("<div class=\"col-md-1\">").append(m.getId()).append("</div>");
                builder.append("<div class=\"col-md-3\">").append(m.getName()).append("</div>");
                builder.append("<div class=\"col-md-1\">").append(m.getRate()).append("</div>");
                builder.append("<div class=\"col-md-2\">").append(m.getDirector()).append("</div>");
                builder.append("<div class=\"col-md-3\">").append(m.getReleaseDate()).append("</div>");
                builder.append("</div>");
                i++;
            }
            out.print(builder.toString());
        %>
    </div>
    <hr>
    <form method="post" action="/movies">
        <div class="row">
            <div class="col-lg-2">
                <span>Title</span>
            </div>
            <div class="col-lg-2">
                <input class="form-control" type="text" name="title">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <span>Rate</span>
            </div>
            <div class="col-lg-2">
                <input class="form-control" type="text" name="rate">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2">
                <span>Director</span>
            </div>

            <div class="col-lg-2">
                <select class="form-control" name="director">
                    <%
                        List<Person> peoples = (List<Person>) request.getAttribute("directors");
                        for (Person p : peoples) {
                            out.println("<option value=\"" + p.getId() + "\" >" + p.getFirstName() + " " + p.getSurname() + "</option>");
                        }
                    %>

                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2">
                Release Date
            </div>
            <div class="col-lg-2">
                <input class="form-control" id="date" name="releaseDate" type="date"/>
            </div>
        </div>
        <input type="submit" value="Add Movie">
    </form>
</div>


<select class="form-control" name="profession">
    <%
        Profession[] professions = Profession.values();
        for (Profession p : professions) {
            out.println("<option value=\"" + p.toString().toUpperCase() + "\">" + p.toString() + "</option>");
        }
    %>
</select>

<jsp:include page="views/footer.jsp"></jsp:include>
</body>
</html>
